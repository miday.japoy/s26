/*//Routing is the way which we handle our client request based on their endpoints.

const http = require('http');

const PORT = 4000;


http.createServer(function(request, response){

	if(request.url == '/hello'){

	response.writeHead(200,{'Content-Type' : 'text/plain'})
	response.end('Hello World');
	} else {

	//this will be our response if an endpoint passed in the client's request is not recognized or there is no designated route for this endpoint.

	response.writeHead(404,{'Content-Type' : 'text/plain'})
	response.end('Page not available');
	}

}).listen(PORT);

console.log(`Server is running at localhost:${PORT}`)
*/

// mini-activity



const http = require('http');

const PORT = 4000;


http.createServer(function(request, response){

	if(request.url == '/homepage'){

	response.writeHead(200,{'Content-Type' : 'text/plain'})
	response.end('This is Homepage');
	} else if 

	(request.url == '/register'){

	response.writeHead(200,{'Content-Type' : 'text/plain'})
	response.end('This is our register Page');
	} else
	{
	response.writeHead(404,{'Content-Type' : 'text/plain'})
	response.end('Error: Page Not Found');
	}

}).listen(PORT);

console.log(`Server is running at localhost:${PORT}`)



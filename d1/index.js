

let http = require("http");
//we use the "require" directive to load Node.js modules
//a module is a software component or part of a program that contains one or more routines
//HTTP is a portocol that allows the fetching of resources such as HTML documents

http.createServer(function (request, response){

//Use the writeHead to:
	//set a status code for the response - a 300 means OK
	//set the content-type of the response as a plain text message
	response.writeHead(200, {'Content-Type' : 'text/plain'});
	//This sends the response with the text content 'Hello World'
	response.end('Hello World');


}).listen(4000)
//A port is a virtual point where network connections start and end
//the server will be assigned to the port 4000

//When server is running, console will print the message.
console.log('Server Running at localhost:4000');